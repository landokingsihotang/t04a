import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");
        boolean validInput = false;
        do {
            try {
                // Bagian input User
                System.out.print("Masukkan pembilang: ");
                int pembilang = scanner.nextInt();
                System.out.print("Masukkan penyebut: ");
                int penyebut = scanner.nextInt();
                 // Menggunakan method pembagian dengan exception
                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasilnya adalah: " + hasil);
                validInput = true;
            } catch (InputMismatchException e) {
                // error ketika pengguna memasukkan nilai bukan int
                System.out.println("Input yang Anda masukkan Harus Interger. Silakan coba lagi.");
                //ketika menemukan kesalah diatas ,maka looping untuk memasukkan angka 
                //akan ada kembali dengan scanner.nextline
                scanner.nextLine();
                 // error ketika pengguna memasukkan nilai penyebut nol
            } catch (ArithmeticException e){
                //method get massage diambil dari parent class exception 
                //sehingga tidak perlu di buat di method pembagian yang telah dibuat
                System.out.println(e.getMessage()); 
            }
            //edit here
        } while (!validInput);

        scanner.close();
    }
    public static int pembagian(int pembilang, int penyebut)  {

        //add exception apabila penyebut bernilai 0   
            if(penyebut == 0){
                throw new ArithmeticException ("Penyebut tidak boleh nol. Silakan coba lagi.");
            }
            else {return pembilang / penyebut;}
 
    }
}
